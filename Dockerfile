FROM ubuntu:18.04
CMD ["bash"]
    #General
WORKDIR /home
EXPOSE 8081

RUN apt update
RUN apt install python3 python3.6 python3-pip git wget net-tools python-setuptools curl automake libsctp-dev python3.8 cmake-curses-gui libpcre2-dev -y
RUN pip3 install numpy requests pyyaml connexion[swagger-ui]

#RIC software dependencies
RUN apt purge --auto-remove cmake -y && apt install software-properties-common lsb-release -y && apt clean all 
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null && \ 
    apt-add-repository "deb https://apt.kitware.com/ubuntu/ $(lsb_release -cs) main" && \
    apt install kitware-archive-keyring && rm /etc/apt/trusted.gpg.d/kitware.gpg && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6AF7F09730B3F0A4 && \
    apt update && apt install cmake -y

#RIC installation to get ahold of the _xapp_sdk.so file for the commRM

RUN apt update && apt install libsctp-dev python3.8 cmake-curses-gui libpcre2-dev python3-dev -y
RUN pip3 install bison 
RUN apt update && apt-get -y install asn1c iputils-ping nano byacc -y

# Dependency for making swig
RUN wget ftp://ftp.gnu.org/gnu/m4/m4-1.4.10.tar.gz && tar -xvzf m4-1.4.10.tar.gz && cd m4-1.4.10 && ./configure --prefix=/usr/local/m4 && make && make install
RUN wget http://ftp.gnu.org/gnu/bison/bison-3.5.tar.gz && tar -xvzf bison-3.5.tar.gz && cd bison-3.5 && PATH=$PATH:/usr/local/m4/bin/  \
    && ./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.5.2 && make && make install
# https://geeksww.com/tutorials/miscellaneous/bison_gnu_parser_generator/installation/installing_bison_gnu_parser_generator_ubuntu_linux.php


RUN git clone https://github.com/swig/swig.git && cd swig/ && \
 #./autogen.sh && ./configure --prefix=/usr/ && apt-get install byacc -y && make -j $(nproc) && make install 
 ./autogen.sh && ./configure --prefix=/usr/ && make && make install 
 
RUN git clone https://gitlab.eurecom.fr/mosaic5g/flexric.git && cd flexric/ && \
    git checkout d44e508cdf76e0d6fa0a0ba2899d427884b74543 && mkdir build && cd build && cmake .. && make &&make install

#    git checkout d44e508cdf76e0d6fa0a0ba2899d427884b74543 && mkdir build && cd build && cmake .. && make -j $(nproc) && make install

ADD /python-flask-server-generated /home/ 
ADD /core /home/swagger_server/core
ADD flexric.conf /home/flexric-template.conf
ADD setup.conf /home/setup.conf

RUN rm /home/bison-3.5.tar.gz && rm /home/m4-1.4.10.tar.gz && rm -rf bison-3.5

ADD commrm_init.sh /home/commrm_init.sh
# Setup of the python-flask server 


RUN python3 /home/setup.py build && python3 /home/setup.py install && pip3 install -r requirements.txt && pip3 install -r test-requirements.txt
RUN cp /home/flexric/build/examples/xApp/python3/_xapp_sdk.so /home/swagger_server/core/_xapp_sdk.so
RUN cat /home/setup.conf