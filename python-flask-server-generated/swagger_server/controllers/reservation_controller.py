import connexion
import six

from swagger_server.models.reservation import Reservation  # noqa: E501
from swagger_server.models.reservation_description import ReservationDescription  # noqa: E501
from swagger_server import util
from ..core import commrm as commRM
#from ..core import xapp_slice_moni_ctrl as commrm

commrm = commRM.communication_manager()



def create_reservation(body):  # noqa: E501
    """Create a new ressource reservation
    
     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: List[Reservation]
    """
    if connexion.request.is_json:
        body = ReservationDescription.from_dict(connexion.request.get_json())  # noqa: E501
        print(type(body))
        print(body.address_type)
        response = commrm.create_reservation(body)
    return response


def delete_reservation(reservation_id):  # noqa: E501
    """Delete a specific ressource reservation

     # noqa: E501

    :param reservation_id: Reference to an existing reservation
    :type reservation_id: str

    :rtype: None
    """
    response = commrm._delete_reservation(reservation_id)
    return response


def get_reservation(reservation_id):  # noqa: E501
    """Get a specific ressource reservation

     # noqa: E501

    :param reservation_id: Reference to an existing reservation
    :type reservation_id: str

    :rtype: Reservation
    """
    response = commrm.get_reservation(reservation_id)
    return response


def get_reservations():  # noqa: E501
    """Get all ressource reservations

     # noqa: E501


    :rtype: List[Reservation]
    """
    response = commrm.get_reservation_all()
    return response
