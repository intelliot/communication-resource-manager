CONTAINER=1
if __name__ == "__main__":
    import templates 

    import xapp_sdk as ric
    import monitor_app
    import util
    import slice_api
    import slicing_methods_api
else: 
    from . import templates 
    from . import xapp_sdk as ric
if CONTAINER == 1:
    from ..core import monitor_app
    from ..core import util
    from ..core import slice_api
    from ..core import slicing_methods_api
    from swagger_server.models.reservation import Reservation
import time
import numpy as np
import threading

# Specifically for rate-based service with an RB allocation made for the slice
class bandwidth_monitor(threading.Thread):
    def __init__(self, mac_cb, calculate_interval, rnti, slice_def, min_throughput):
        super().__init__()
        self.current_rbs = 0
        self.slice_def=slice_def
        self.rbs_needed = self.slice_def["slice_algo_params"]['pos_high'] - self.slice_def["slice_algo_params"]['pos_low']
        self.thread_active=1
        self.mac_cb = mac_cb
        self.rnti=rnti
        self.min_throughput=min_throughput
        self.test={}
        
    def run(self):
        while(self.thread_active):
            time.sleep(0.02) # otherwise the world falls apart!
            self.rbs_needed = util.get_rbs_needed(self.mac_cb[str(self.rnti)]['dl_mcs1'], self.min_throughput)
            self.test['cqi'] = self.mac_cb[str(self.rnti)]['wb_cqi']
    def modify_rate(self, rate):
        
        self.min_throughput = rate

    def shutdown(self):
        #print("shutdown here")
        self.thread_active=0

# Overall thread which monitors the performance of different services and updates allocation if it looks like the service is not being met
class monitor_reservations(threading.Thread): 
    def __init__(self, update_interval, handles, slice_def, mac_cb, slice_api_handle, ue_struct, node_idx, slicing_method, slicing_method_name):
        super().__init__()
        self.handles = handles
        self.thread_active = 1
        self.update_interval = update_interval
        self.slice_def = slice_def
        self.mac_cb = mac_cb
        self.slice_api_handle=slice_api_handle
        self.ue_struct = ue_struct
        self.node_idx = node_idx
        self.slicing_method = slicing_method
        self.slicing_method_name = slicing_method_name


    def run (self):
        while(self.thread_active):
            time.sleep(self.update_interval)
            for handle in self.handles:
                if self.handles[str(handle)]['service']== 'static_bitrate' :
                    # check if the required resource blocks are allocated, if not we modify the slice and allocate the RBs required
                    if self.handles[str(handle)]['rate'].current_rbs != self.handles[str(handle)]['rate'].rbs_needed:
                        self.handles[str(handle)]['rate'].current_rbs = self.handles[str(handle)]['rate'].rbs_needed
                        slice_dict = self.slicing_method.modify(1, self.handles[str(handle)]['rate'].current_rbs, self.slice_def)
                        self.slicing_method.order_reservations(slice_dict)
                        self.slice_api_handle.post_slice(slice_dict, self.node_idx)
                        self.slice_api_handle.assoc_ue(self.ue_struct[str(handle)], self.node_idx)
                        mcs =  self.mac_cb[str(self.handles[str(handle)]['rate'].rnti)]['dl_mcs1']
                    # elif self.handles[str(handle)]['slicing'] == 'nvs':
                    #     print("do something")

                    #print('current rbs : ', int(self.handles[str(handle)]['rate'].current_rbs), " throughput : ", util.get_available_throughput(mcs,self.handles[str(handle)]['rate'].current_rbs*4)*1000, 'mcs :', mcs ,'\n')
        return
    def shutdown(self):
        for handle in self.handles:
            if self.handles[str(handle)]['service'] == 'vr_latency':
              self.handles[str(handle)]['vr_latency'].shutdown()
        self.thread_active=0
        return


# Simplified version to add UEs to a slice. Only the first UE can be added - will need to change this in the future when multiple slices exist
def get_ue_id(type, value, mac_cb):
    #TODO Retrieve rnti based on IP, or MAC
    # Takes the first RNTI, which means the first UE to connect always - this can be changed
    rntis = list(mac_cb.stats.keys())
    print(rntis)
    rnti = rntis[0] 
    ue_rnti = mac_cb.stats[str(rnti)]['rnti']
    return ue_rnti


class communication_manager(object): # Backend functions and logic for handling requests

    def __init__(self):
        
        ric.init()
        super().__init__()
        configuration = {'reference_rate': 50, 'min_best_effort_rate': 5, 'max_rbs':25, 'min_best_effort_rbs': 4, 'slicing_method': 'NVS', 'update_interval': 1}

        config_file = '/home/setup_fixed.conf'
        f=open(config_file, 'r')
        content = f.readlines()
        for c in content:
            input = c.split(' ')
            if input[0] in configuration:
                configuration[input[0]] = input[1].rstrip()


        # Not optimal but some of these are simply set to work with
        #self.reference_rate = 45 #testing with NVS
        #TODO how to acquire the reference rate?
        #self.max_rbs = 24
        self.slicing_method_name = "NVS" #configuration['slicing_method'] #'static_rbs'
        self.slice_resource= configuration['reference_rate']
        self.update_interval = int(configuration['update_interval'])
        self.reference_rate = int(configuration['reference_rate'])
        self.min_best_effort = int(configuration['min_best_effort_rate'])

        self.conn = ric.conn_e2_nodes()
        self.node_idx = 0

        ############# method_names_allowed = ['static', 'nvs','static_rbs', 'edf'] ################
        # Leveraging other exsisting xApps. We procure a handle for these and pass to relevant functions
        self.monitor_handle = monitor_app.monitor() 
        self.slice_api_handle = slice_api.api(self.conn)

        # Generic storage for different definitions used by different functions.
        self.slice_ues={}
        self.slice_definitions = {} # id , definition
        self.reservations = {}

        # Storage of objects - required for accessing running threads and shutting these down.
        self.handles = {}

        # not always running it
        self.reservation_monitor_handle_active =0
        #For saving information in file for each UE - only some mac level at the moment
        self.saving = 0
        if self.saving == 1:
            self.save_rate = 1
            file_name = "RANstats_MAC"
            self.saving_data_handle = util.save_data(self.monitor_handle.mac_cb, self.save_rate ,2000, file_name)
            self.saving_data_handle.start()

        # Intended to deal with different types of slicing mechanisms being used.
        
        self.slicing_method_name = 'NVS'
        print("\n We have a NVS slice setup \n")
        self.slice_def=slicing_methods_api.nvs_slicing.get_clean_slice(self.reference_rate) #templates.slice_def_static.get_clean_slice()
        #self.slice = self.slice_def
        self.slicing_method=slicing_methods_api.nvs_slicing(self.reference_rate, self.monitor_handle, self.slice_def, self.slice_definitions, self.min_best_effort)

        print('initial self.slice\n',self.slice_def)
        
        if self.slicing_method_name == 'static' or self.slicing_method_name == 'static_rbs':
            self.reservation_monitor_handle_active = 1
            self.reservation_monitor_handle = monitor_reservations(self.update_interval, self.handles, self.slice_def, self.monitor_handle.mac_cb.stats, self.slice_api_handle, self.slice_ues, self.node_idx, self.slicing_method, self.slicing_method_name)
            self.reservation_monitor_handle.start()

    def reread_config(self):
        configuration = {'reference_rate': 50, 'min_best_effort_rate': 5, 'max_rbs':25, 'min_best_effort_rbs': 4, 'slicing_method': 'nvs', 'update_interval': 1}
        config_file = 'setup_fixed.conf'
        f=open(config_file, 'r')
        content = f.readlines()

        for c in content:
            input = c.split(' ')
            if input[0] in configuration:
                configuration[input[0]] = input[1].rstrip()



        # Not optimal but some of these are simply set to work with
        #self.reference_rate = 45 #testing with NVS
        #TODO how to acquire the reference rate?
        #self.max_rbs = 24
        self.slicing_method_name = configuration['slicing_method'] #'static_rbs'
        self.slice_resource = -1
        if self.slicing_method_name == 'static_rbs' or self.slicing_method_name == 'static':
            self.slice_resource = int(configuration['max_rbs'])
        elif self.slicing_method_name =='nvs':
            self.slice_resource=configuration['reference_rate']
        #self.update_interval = configuration['update_interval'] # need to fix
        self.reference_rate = int(configuration['reference_rate'])
        self.min_best_effort = int(configuration['min_best_effort_rate'])
        self.slice_def=slicing_methods_api.nvs_slicing.get_clean_slice(self.reference_rate) #templates.slice_def_static.get_clean_slice()
        self.slicing_method=slicing_methods_api.nvs_slicing(self.reference_rate, self.monitor_handle, self.slice_def, self.slice_definitions,self.min_best_effort)
        self._reset_reservations()




    def _post_slice (self, slice_def): # POST reservation - API calls
        self.slice_api_handle.post_slice(slice_def, self.node_idx)
        return

    def _assoc_ue(self, ue_def):
        self.slice_api_handle.assoc_ue(ue_def, self.node_idx)

    def _delete_reservation (self, id): # DELETE reservation - API calls
        slice_exists = 0
        for slice_index in range(len(self.slice_def["slices"])):
            if int(id) == self.slice_def["slices"][slice_index]["id"]:
                slice_exists = 1
                break
        if slice_exists:
            self.slice_api_handle.delete_slice(id, self.node_idx)
            del self.slice_def["slices"][slice_index]
            if self.slicing_method_name == 'static' or self.slicing_method_name =='static_rbs':
                del self.handles[str(id)]
            del self.reservations[str(id)] #TODO better way to handle local reservations
            self.slice_def['num_slices']=len(self.slice_def["slices"])
            self.slicing_method.order_reservations(self.slice_def)
        return

    def _reset_reservations (self): # DELETE reservation - API calls
        self.slice_api_handle.reset_reservations(self.node_idx)
        for handle in self.handles:
            if 'rate' in self.handles[str(handle)]:
                self.handles[str(handle)]['rate'].thread_active=0
        if self.slicing_method_name == 'static' or self.slicing_method_name == 'static_rbs':
            self.slice_def=slicing_methods_api.static_slicing.get_clean_slice(self.slice_resource) #templates.slice_def_static.get_clean_slice()
        elif self.slicing_method_name == 'nvs':
            self.slice_def=slicing_methods_api.nvs_slicing.get_clean_slice(self.reference_rate)
        #self.slice_def = self.slicing_method.get_clean_slice(self.reference_rate) #templates.slice_def_static.get_clean_slice()

        print(self.slice_def)
        self.slicing_method.reset_slice(self.slice_def)
        

        return

    def get_reservation(self,id): # GET reservation - from locally held
        if id in self.reservations:
            resp = self.reservations[str(id)]
            return resp 
        else:
            print("ID does not exist, cannot return")
            return

    def get_reservation_all(self): #GET reservation_all - from locally held
        print("Returned all active slices")
        resp = []
        for id in self.reservations:
            resp.append(self.reservations[str(id)])
        print(resp)
        return resp

    def create_reservation (self, params) : # POST reservation
        if len(self.reservations) < 2:
            service = 'rate'
            ue_rnti = get_ue_id(params.address_type, params.client_address, self.monitor_handle.mac_cb)
            print(self.slice_def)
            self.slice_def, slice_id = self.slicing_method.generate_slice_def(int(params.min_throughput_mbps_downlink), int(params.min_throughput_mbps_uplink), str(ue_rnti)) #TODO extend to multiple variables
            print(self.slice_def, '\n', slice_id)
            if slice_id == -1:
                return -1
            ue_def = util.ue_assoc_def(slice_id, ue_rnti)
            self._post_slice(self.slice_def)
            self._assoc_ue(ue_def)
            
            
            self.slice_ues[str(slice_id)] = ue_def

            handle = {}
            handle['slicing'] = self.slicing_method_name 
            # Start thread depending on service requested e.g. VR latency or bitrate
            if service == 'rate':
                if self.slicing_method_name == 'static' :
                    handle['service'] = self.slicing_method_name
                    handle['rate']= bandwidth_monitor(mac_cb=self.monitor_handle.mac_cb.stats, calculate_interval=1, rnti=str(ue_rnti), slice_def = self.slice_def['slices'][slice_id], min_throughput = params.reservation["minThroughputMbpsDownlink"])
                    handle['rate'].start()
                # if nvs we don't need to monitor

            #### Not implemented features #####
            params.max_latency_millis_uplink = -1 # Until we can infer on these
            params.max_jitter_millis_uplink = -1
            params.max_latency_millis_downlink = -1
            params.max_jitter_millis_downlink = -1
            params.min_throughput_mbps_uplink = -1
            #### Implemented features #####
            params.reservationId = str(slice_id)
            params.state = "reserved"
            reservation = Reservation(params,str(slice_id), "reserved")
            self.reservations[str(slice_id)]=reservation
            return reservation
        else:
            print("can only allow 3 slices, otherwise app may crash")
            return -1

    def proxy_modify(self, slice_id, new_rate):
        if self.slicing_method_name == 'static' :
            self.handles[str(slice_id)]['rate'].modify_rate(new_rate)
        if self.slicing_method_name == 'static_rbs':
            self.slicing_method.modify(slice_id, new_rate, self.slice_def)
        if self.slicing_method_name == 'nvs':
             self.slicing_method.modify(slice_id, new_rate, self.slice_def)
             print("yay")
        print(self.slice_def)
        self.slicing_method.order_reservations(self.slice_def)
        self.slice_api_handle.post_slice(self.slice_def, self.node_idx)
        self.slice_api_handle.assoc_ue(self.slice_ues[str(slice_id)], self.node_idx)

    
        return


    def graceful_shutdown(self):
        if self.reservation_monitor_handle_active == 1:
            self.reservation_monitor_handle.shutdown()
        for handle in self.handles:
            if 'rate' in self.handles[str(handle)]:
                self.handles[str(handle)]['rate'].shutdown()
            if 'vr_latency' in self.handles[str(handle)]:
                self.handles[str(handle)]['vr_latency'].shutdown()
        time.sleep(1)
        print("monitor")
        self.monitor_handle.shutdown()
        print("slice_cb")
        self.slice_api_handle.shutdown()
        print("Everything should be shutdown correctly now")
        if self.saving==1:
            self.saving_data_handle.shutdown()
        return


def input_paser(input):
    try: 
        output = str.split(input)
    except:
        return output
    return output


if __name__ == "__main__": 
    ric.init()
    commrm = communication_manager()
    reservation_generator = templates.reserve()
    while(1):
        command = input("Type command value\n")
        output = input_paser(command)
        if len(output) == 2:
            command, value = output[0], output[1]
        elif len(output) == 3:
            command, value, value2 = output[0], output[1], output[2]

        if command == "HELP":
            print("Commands are : HELP, ADD RBs/bandwidth, DEL $slice_id, RESET, GET $slice_id, GET_ALL, EXIT, TEST, CONFIG, ASSOC $slice_id, MODIFY $new_rate. Value is the ID for slices, or RBs, when relevant\n")

        if command == "DEL":
            commrm._delete_reservation(value)
            print("Delete slice", value,"\n")

        if command == "ADD":
            # mimic the reservation json
            params = reservation_generator.form()
            params.reservation["minThroughputMbpsDownlink"]=value
            if len(output) == 3:
                params.reservation["maxLatencyMillisDownlink"]=value2
            params.reservation["addressType"]="ipAddress"
            params.reservation["clientAddress"] = "172.16.0.4"
            output = commrm.create_reservation(params)
            
        if command == "CONFIG":
            commrm.reread_config()

        if command == "RESET":
            commrm._reset_reservations()
            print("Reset slices\n")
        
        if command == "GET":
            out = commrm.get_reservation(value)
            print(out,"\n")
        
        if command == "GET_ALL":
            commrm.get_reservation_all()

        if command == "TEST":
            commrm.test()

        if command == "ASSOC":
            ue_def = commrm.ue_assoc_def("IP", "172.16.0.4", value)
            commrm._assoc_ue(ue_def)
            

        if command == "EXIT":
            commrm.graceful_shutdown()
            exit()

        if command == 'MODIFY':
            commrm.proxy_modify(1, int(value))



