
#Kbits per RB (27 x 110), so for uplink

if __package__ or "." in __name__:
    from . import templates
else:
    import templates

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__


add_static_slices = {
    "num_slices" : 3,
    "slice_sched_algo" : "STATIC",
    "slices" : [
        {
            "id" : 0,
            "label" : "s1",
            "ue_sched_algo" : "PF",
            "slice_algo_params" : {"pos_low" : 0, "pos_high" : 2},
        },
        {
            "id" : 2,
            "label" : "s2",
            "ue_sched_algo" : "PF",
            "slice_algo_params" : {"pos_low" : 3, "pos_high" : 10},
        },
        {
            "id" : 5,
            "label" : "s3",
            "ue_sched_algo" : "PF",
            "slice_algo_params" : {"pos_low" : 11, "pos_high" : 13},
        }
    ]
}


reset_slices = {
    "num_slices" : 0
}


delete_slices = {
    "num_dl_slices" : 1,
    "delete_dl_slice_id" : [5]
}


assoc_ue_slice = {
    "num_ues" : 1,
    "ues" : [
        {
            "rnti" : 0, # TODO: get rnti from slice_ind_to_dict_json()
            "assoc_dl_slice_id" : 0
        }
    ]
}



static_slice= {
            "num_slices" : 1,
            "slice_sched_algo" : "STATIC",
            "slices" : [
                {
                    "id" : 0,
                    "label" : "s1",
                    "ue_sched_algo" : "PF",
                    "slice_algo_params" : {"pos_low" : 0, "pos_high" : 24},
                }
            ]
        }



add_nvs_slices_rate = {
    "num_slices" : 1,
    "slice_sched_algo" : "NVS",
    "slices" : [
        {
            "id" : 0,
            "label" : "s1",
            "ue_sched_algo" : "PF",
            "type" : "SLICE_SM_NVS_V0_RATE",
            "slice_algo_params" : {"mbps_rsvd" : 60, "mbps_ref" : 120},
        }
    ]
}





class reserve: # Basic form we store slice definitions in


    def form(self):
    
        self.reservation = {
        "client_address": 0,
        "address_type": 0,
        "max_latency_millis_uplink": 0,
        "max_jitter_millis_uplink":0,
        "min_throughput_mbps_uplink": 0,
        "max_latency_millis_downlink": 0,
        "max_jitter_millis_downlink": 0,
        "min_throughput_mbps_downlink": 0
        }
        self.reservationId= "string"
        self.state = "unreserved" 
        return self


#   def reservationformdebug(self):
#     self.reservation={
#       "clientAddress": 0,
#       "addressType": 0,
#       "maxLatencyMillisUplink": 0,
#       "maxJitterMillisUplink":0,
#       "minThroughputMbpsUplink": 0,
#       "maxLatencyMillisDownlink": 0,
#       "maxJitterMillisDownlink": 0,
#       "minThroughputMbpsDownlink": 0
#     }
#     self.reservationId = "string"
#     self.state = "unreserved" 
#     return self

class slice_def_static:
    def get_clean_slice():
        static_slice_def= {
            "num_slices" : 1,
            "slice_sched_algo" : "STATIC",
            "slices" : [
                {
                    "id" : 0,
                    "label" : "s1",
                    "ue_sched_algo" : "PF",
                    "slice_algo_params" : {"pos_low" : 0, "pos_high" : 24},
                }
            ]
        }
        return static_slice_def