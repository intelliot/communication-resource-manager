CONTAINER=1 
from re import I
import time
from datetime import datetime

#if __name__=='__main__':
if CONTAINER == 1:
    
   from ..core import xapp_sdk as ric
else:
    import xapp_sdk as ric


####################
#### MAC INDICATION CALLBACK
####################

class monitor():
#make as a thread

    def __init__(self):
        super().__init__()
        #t=ric.init()
        #mac_stats = mac_stats.mac_stats()
        #if __name__=='__main__':
        self.data_storage = 1000
        self.safety = 2

        self.conn = ric.conn_e2_nodes()
        #self.pdcp_cb = PDCPCallback()
        self.mac_cb = MACCallback()
        self.rlc_cb = RLCCallback()


        for i in range(0, len(self.conn)):
            print("mac handle!")
            self.MAC_handle = ric.report_mac_sm(self.conn[i].id, ric.Interval_ms_1, self.mac_cb)
            #self.RLC_handle = ric.report_rlc_sm(self.conn[i].id, ric.Interval_ms_1, self.rlc_cb)
            #self.PDCP_handle = ric.report_pdcp_sm(self.conn[i].id, ric.Interval_ms_1, self.pdcp_cb)
            
    def shutdown(self):
        time.sleep(1)


        ric.rm_report_mac_sm(self.MAC_handle)
        #ric.rm_report_rlc_sm(self.RLC_handle)
         


def update_mac_index (ind, i): # TODO move into the monitor_app

    index = {}

    index['dl_aggr_tbs'] = ind.ue_stats[i].dl_aggr_tbs  
    index['ul_aggr_tbs'] = ind.ue_stats[i].ul_aggr_tbs 
    index['wb_cqi'] = ind.ue_stats[i].wb_cqi
    index['dl_mcs1'] = ind.ue_stats[i].dl_mcs1
    index['ul_mcs1'] = ind.ue_stats[i].ul_mcs1
    index['dl_aggr_sdus'] = ind.ue_stats[i].dl_aggr_sdus
    index['dl_num_harq']       =ind.ue_stats[i].dl_num_harq
    index['bsr']       =ind.ue_stats[i].bsr

    return index

def init_ue_mac_stat_mem(mem_size):
    mem={'dl_aggr_tbs':[0]*mem_size,'ul_aggr_tbs':[0]*mem_size,'wb_cqi':[0]*mem_size,'dl_mcs1':[0]*mem_size, 'ul_mcs1':[0]*mem_size, 'clock':[0]*mem_size, 'dl_aggr_sdus': [0]*mem_size, 'dl_num_harq': [0]*mem_size, 'bsr': [0]*mem_size}
    return mem

def update_rlc_index (ind, i): # TODO move into the monitor_app

    index = {}


    index['txpdu_pkts'] = ind.rb_stats[i].txpdu_pkts  
    index['txpdu_wt_ms']       =ind.rb_stats[i].txpdu_wt_ms
    index['txpdu_bytes']       =ind.rb_stats[i].txpdu_bytes
    index['txsdu_pkts']        =ind.rb_stats[i].txsdu_pkts
    index['txsdu_bytes']       =ind.rb_stats[i].txsdu_bytes

    return index



def init_ue_rlc_stat_mem(mem_size):
    mem={'txpdu_pkts':[0]*mem_size,'txpdu_wt_ms':[0]*mem_size,'txpdu_bytes':[0]*mem_size,'txsdu_pkts':[0]*mem_size, 'txsdu_bytes':[0]*mem_size, 'clock':[0]*mem_size}
    return mem


def update_list(_list, input, counter):
    
    for index in input.keys():

        val=input[str(index)]
        _list[str(index)][counter]=val
    return 


#  MACCallback class is defined and derived from C++ class mac_cb

############ Info on MAC_SM                         https://hackmd.io/@lfetman/SkgHNW90c           ################
class MACCallback(ric.mac_cb):
    # Define Python class 'constructor'
    def __init__(self):
        # Call C++ base class constructor

        self.stats={}
        self.counter = 0
        #self.f = open("60fps.csv", "w")
        name = file_name +'_mac.csv'
        #self.f = open(name, "w")
        #self.f.write('dl_aggr_tbs ' + '1ms\n')
        self.running = 1
        self.new_data = 0
        ric.mac_cb.__init__(self)
        self.stats = {}
        self.mem_size = 5000
        self.test = {}
        self.rntis = []
        self.start = time.time()


    # Override C++ method: virtual void handle(swig_mac_ind_msg_t a) = 0;
    def handle(self, ind):
        if self.running == 1:
            if len(ind.ue_stats) > 0:
                
                n = len(ind.ue_stats)
                for i in range(n):
                    stat = {}
                    
                    rnti = ind.ue_stats[i].rnti
                    if rnti not in self.rntis:
                       #self.test[str(rnti)] = [{}]*self.mem_size
                       self.test[str(rnti)] = init_ue_mac_stat_mem(self.mem_size)
                       self.rntis.append(rnti)
                    stat = {}
                    
                    rnti = ind.ue_stats[i].rnti


                    stat['dl_aggr_tbs']       =ind.ue_stats[i].dl_aggr_tbs        #Changes with ping - Downlink total bytes?
                    stat['ul_aggr_tbs']       =ind.ue_stats[i].ul_aggr_tbs       #Fails? - Uplink Total bytes?
                    stat['dl_aggr_bytes_sdus']=ind.ue_stats[i].dl_aggr_bytes_sdus #changes with ping  Downlink Service Data Unit
                    stat['ul_aggr_bytes_sdus']=ind.ue_stats[i].ul_aggr_bytes_sdus #Changes with ping - Uplink Service Data Unit
                    stat['pusch_snr']         =ind.ue_stats[i].pusch_snr          #Set?
                    stat['pucch_snr']         =ind.ue_stats[i].pucch_snr          #Set?
                    stat['rnti']              =ind.ue_stats[i].rnti               #RNTI for UE[i] 
                    stat['dl_aggr_prb']       =ind.ue_stats[i].dl_aggr_prb        #Changes with ping
                    stat['ul_aggr_prb']       =ind.ue_stats[i].ul_aggr_prb        #Always increases
                    stat['dl_aggr_sdus']      =ind.ue_stats[i].dl_aggr_sdus       #set?
                    stat['ul_aggr_sdus']      =ind.ue_stats[i].ul_aggr_sdus       #set?
                    stat['dl_aggr_retx_prb']  =ind.ue_stats[i].dl_aggr_retx_prb   #set?
                    stat['ul_aggr_retx_prb']  =ind.ue_stats[i].ul_aggr_retx_prb   #set?
                    stat['wb_cqi']            =ind.ue_stats[i].wb_cqi             #set?
                    stat['dl_mcs1']           =ind.ue_stats[i].dl_mcs1            #set?
                    stat['ul_mcs1']           =ind.ue_stats[i].ul_mcs1            #set?
                    stat['dl_mcs2']           =ind.ue_stats[i].dl_mcs2            #set?
                    stat['ul_mcs2']           =ind.ue_stats[i].ul_mcs2            #set?
                    stat['phr']               =ind.ue_stats[i].phr                #Set?
                    stat['bsr']               =ind.ue_stats[i].bsr                #Random? the buffer status reporting, very interesting
                    stat['dl_num_harq']       =ind.ue_stats[i].dl_num_harq        #Set?
                    self.counter = (self.counter + 1) % self.mem_size
                    self.new_data = 1
                    self.stats[str(rnti)] = stat
                    new_data = update_mac_index(ind, i)
                    new_data['clock'] = (time.time() - self.start) *1000 #millisecond
                    update_list(self.test[str(rnti)], new_data, self.counter) # attempt at updating dict list
                    #self.test[str(rnti)][self.counter] = new_data

                    #self.stats[str(rnti)][self.counter%self.mem_size]=new_stat
                    #now = datetime.now().time()
                    #print(now)
                    #print(stat['rnti'], " : ", stat['wb_cqi'] , ' : ', now)

                    #self.f.write(str(stat['dl_aggr_tbs']) + ', ' + str(stat['dl_mcs1'])  + "\n")

                    #if live_logging == True:
                        #print('MAC: ', 'UE', stat.mac_stats['rnti'] , ' Rx data : ' ,stat.mac_stats['dl_aggr_tbs'])

                    #print("MAC",self.stats[str(rnti)],"\n")



####################
#### RLC INDICATION CALLBACK
####################

class RLCCallback(ric.rlc_cb):
    # Define Python class 'constructor'
    def __init__(self):
        # Call C++ base class constructor
        ric.rlc_cb.__init__(self)
        self.stats={}
        self.counter = 0
        self.mem_size = 5000
        self.test = {}
        self.rntis = []
        self.rntis = []
        self.start = time.time()
        self.file = '/home/orandesktop/project/testlog_rlc.csv'
        #self.f = open(file, 'a')
        
    # Override C++ method: virtual void handle(swig_rlc_ind_msg_t a) = 0;
    def handle(self, ind):
        # Print swig_rlc_ind_msg_t
        if len(ind.rb_stats) > 0:
            n = len(ind.rb_stats)
            for i in range(n):
                stat = {}
                
                rnti = ind.rb_stats[i].rnti
                if rnti not in self.rntis:
                    #self.test[str(rnti)] = [{}]*self.mem_size
                    self.test[str(rnti)] = init_ue_rlc_stat_mem(self.mem_size)
                    self.rntis.append(rnti)
                stat = {}

                rnti = ind.rb_stats[i].rnti
                stat['txpdu_pkts']        =ind.rb_stats[i].txpdu_pkts
                stat['txpdu_bytes']       =ind.rb_stats[i].txpdu_bytes
                stat['txpdu_wt_ms']       =ind.rb_stats[i].txpdu_wt_ms
                stat['txpdu_dd_pkts']     =ind.rb_stats[i].txpdu_dd_pkts
                stat['txpdu_dd_bytes']    =ind.rb_stats[i].txpdu_dd_bytes
                stat['txpdu_retx_pkts']   =ind.rb_stats[i].txpdu_retx_pkts
                stat['txpdu_retx_bytes']  =ind.rb_stats[i].txpdu_retx_bytes
                stat['txpdu_segmented']   =ind.rb_stats[i].txpdu_segmented
                stat['txpdu_status_pkts'] =ind.rb_stats[i].txpdu_status_pkts
                stat['txpdu_status_bytes']=ind.rb_stats[i].txpdu_status_bytes   
                stat['txbuf_occ_bytes']   =ind.rb_stats[i].txbuf_occ_bytes # I see change
                stat['txbuf_occ_pkts']    =ind.rb_stats[i].txbuf_occ_pkts #comment
                #stats['txbuf_wd_ms']       =ind.rb_stats[i].txbuf_wd_ms # outcommented
                # /* RX */
                stat['rxpdu_pkts']        =ind.rb_stats[i].rxpdu_pkts
                stat['rxpdu_bytes']       =ind.rb_stats[i].rxpdu_bytes
                stat['rxpdu_dup_pkts']    =ind.rb_stats[i].rxpdu_dup_pkts
                stat['rxpdu_dup_bytes']   =ind.rb_stats[i].rxpdu_dup_bytes
                stat['rxpdu_dd_pkts']     =ind.rb_stats[i].rxpdu_dd_pkts
                stat['rxpdu_dd_bytes']    =ind.rb_stats[i].rxpdu_dd_bytes
                stat['rxpdu_ow_pkts']     =ind.rb_stats[i].rxpdu_ow_pkts
                stat['rxpdu_ow_bytes']    =ind.rb_stats[i].rxpdu_ow_bytes
                stat['rxpdu_status_pkts'] =ind.rb_stats[i].rxpdu_status_pkts
                stat['rxpdu_status_bytes']=ind.rb_stats[i].rxpdu_status_bytes
                # stat.rlc_stats['rxpdu_rotout_ms']   =ind.ue_stats[i].rxpdu_rotout_ms #not implemented
                # print("test01")
                # stat.rlc_stats['rxpdu_potout_ms']   =ind.ue_stats[i].rxpdu_potout_ms #not implemented
                # print("test01")
                # stat.rlc_stats['rxpdu_sptout_ms']   =ind.ue_stats[i].rxpdu_sptout_ms #not implemented
                # print("test01")
                stat['rxbuf_occ_bytes']   =ind.rb_stats[i].rxbuf_occ_bytes # comment
                stat['rxbuf_occ_pkts']    =ind.rb_stats[i].rxbuf_occ_pkts #comment
                # /* SDU stats */
                # /* TX */
                stat['txsdu_pkts']        =ind.rb_stats[i].txsdu_pkts
                stat['txsdu_bytes']       =ind.rb_stats[i].txsdu_bytes
                # /* RX */
                stat['rxsdu_pkts']        =ind.rb_stats[i].rxsdu_pkts # comment
                stat['rxsdu_bytes']       =ind.rb_stats[i].rxsdu_bytes #comment
                stat['rxsdu_dd_pkts']     =ind.rb_stats[i].rxsdu_dd_pkts
                stat['rxsdu_dd_bytes']    =ind.rb_stats[i].rxsdu_dd_bytes
                stat['rnti']              =ind.rb_stats[i].rnti #comment
                stat['mode']              =ind.rb_stats[i].mode
                stat['rbid']              =ind.rb_stats[i].rbid
                self.counter = (self.counter + 1) % self.mem_size
                self.new_data = 1
                self.stats[str(rnti)] = stat
                new_data = update_rlc_index(ind, i)
                new_data['clock'] = (time.time() - self.start) *1000 #millisecond
                update_list(self.test[str(rnti)], new_data, self.counter) # attempt at updating dict list

                                

                
               




####################
#### PDCP INDICATION CALLBACK
####################

########    INFO ON PDCP       https://hackmd.io/@lfetman/r1S0co3Rc#        #########
class PDCPCallback(ric.pdcp_cb):
    # Define Python class 'constructor'
    def __init__(self):
        # Call C++ base class constructor
        self.stats={}
        self.counter = 0
        name = file_name +'_pdcp.csv'
        #self.f = open(name, "w")
        #self.f.write('dl_aggr_tbs ' + '1ms\n')
        self.running = 1
        ric.pdcp_cb.__init__(self)
        
   # Override C++ method: virtual void handle(swig_pdcp_ind_msg_t a) = 0;
    def handle(self, ind):
        if self.running == 1:
            if len(ind.rb_stats) > 0:
                n=len(ind.rb_stats) 
                
                for i in range(n):
                    stat = {}
                    rnti = ind.rb_stats[i].rnti 
                    stat['txpdu_bytes']          =ind.rb_stats[i].txpdu_bytes    #Changes with ping - Aggregate Tx bytes
                    stat['txpdu_pkts']           =ind.rb_stats[i].txpdu_pkts    #Fails - Number of TX packets
                    stat['txpdu_sn']             =ind.rb_stats[i].txpdu_sn       #changes with ping - Latest sequence number of TX
                    stat['rxpdu_bytes']          =ind.rb_stats[i].rxpdu_bytes    #changes with ping - Aggregate Rx bytes
                    stat['rxpdu_pkts']           =ind.rb_stats[i].rxpdu_pkts     #changes with ping - Number of RX packets received
                    stat['rxpdu_sn']             =ind.rb_stats[i].rxpdu_sn       #Changes with ping - Latest RX sequence number
                    stat['rxpdu_oo_pkts']        =ind.rb_stats[i].rxpdu_oo_pkts  #Set? - Received out-of-order pdus
                    stat['rxpdu_oo_bytes']       =ind.rb_stats[i].rxpdu_oo_bytes #Fails  - aggregated amount of out-of-order rx bytes
                    stat['rxpdu_dd_pkts']        =ind.rb_stats[i].rxpdu_dd_pkts  #Set? - aggregated number of duplicated discarded packets (or dropped packets because of other reasons such as integrity failure) (or RX_DELIV)
                    stat['rxpdu_dd_bytes']       =ind.rb_stats[i].rxpdu_dd_bytes #set? - aggregated amount of discarded packets' bytes 
                    stat['rxpdu_ro_count']       =ind.rb_stats[i].rxpdu_ro_count #set? - this state variable indicates the COUNT value following the COUNT value associated with the PDCP Data PDU which triggered t-Reordering. (RX_REORD)
                    stat['txsdu_pkts']           =ind.rb_stats[i].txsdu_pkts     #set? - number of SDUs delivered
                    stat['txsdu_bytes']          =ind.rb_stats[i].txsdu_bytes    #set? - number of bytes of SDUs delivered
                    stat['rxsdu_bytes']          =ind.rb_stats[i].rxsdu_bytes    #Set? - number of bytes of SDUs received
                    stat['rxsdu_pkts']           =ind.rb_stats[i].rxsdu_pkts    #Fails - number of SDUs received
                    stat['rnti']                 =ind.rb_stats[i].rnti           #RNTI - UE[i] RNTI
                    stat['mode']                 =ind.rb_stats[i].mode           #set? - PDCP AM, 1: PDCP UM, 2: PDCP TM
                    #stat.pdpc_stats['rbid']                 =ind.rb_stats[i].rbid          #Fails
                    #stats[i] = stat
                    self.stats[str(rnti)]=stat
                    self.counter = self.counter + 1
                    #self.f.write(str(stat['txpdu_bytes']) + "\n")


rbs='10rbs/'
fps="30"
test_num=2
file_name = 'newlog/RAN_measure_'+rbs+'test' +str(test_num) +'_' +'1ms_'+fps+'fps'
logging = False
####################
####  GENERAL 
####################
live_logging = 0
offline_logging=0




if __name__=='__main__':
    mac_logging=True
    rlc_logging=False
    pdcp_logging=False
    live_logging=1
    ric.init()
    time.sleep(1)
    monitor_handle=monitor()


    cur_data = 0
    last_data = 0

    time.sleep(1)
    MAC_cur_dl = {}
    MAC_old_dl = {}
    MAC_cur_ul = {}
    MAC_old_ul = {}

    PDCP_cur_tx = {}
    PDCP_old_tx = {}
    PDCP_cur_rx = {}
    PDCP_old_rx = {}

    s=1000

    runtime = 60 # seconds
    update_interval = 10/s # ms

    for stat in monitor_handle.mac_cb.stats:
        #print("test")
        MAC_old_dl[str(stat)] = monitor_handle.mac_cb.stats[str(stat)]['dl_aggr_tbs']
        MAC_old_ul[str(stat)] = monitor_handle.mac_cb.stats[str(stat)]['ul_aggr_tbs']

    for stat in monitor_handle.pdcp_cb.stats:
        PDCP_old_tx[str(stat)]=monitor_handle.pdcp_cb.stats[str(stat)]['txpdu_bytes']
        PDCP_old_rx[str(stat)]=monitor_handle.pdcp_cb.stats[str(stat)]['rxpdu_bytes']

    for i in range(int(runtime*1/update_interval)):
        for stat in monitor_handle.mac_cb.stats:
            #print('Now printing MAC for RNTI', stat,'\n',monitor_handle.mac_cb.stats[str(stat)])
            MAC_cur_dl[str(stat)] = monitor_handle.mac_cb.stats[str(stat)]['dl_aggr_tbs']
            MAC_cur_ul[str(stat)] = monitor_handle.mac_cb.stats[str(stat)]['ul_aggr_tbs']

        for stat in monitor_handle.pdcp_cb.stats: 
            PDCP_cur_tx[str(stat)]=monitor_handle.pdcp_cb.stats[str(stat)]['txpdu_bytes']
            PDCP_cur_rx[str(stat)]=monitor_handle.pdcp_cb.stats[str(stat)]['rxpdu_bytes']


            #print(stat)
        #for stat in monitor_handle.pdcp_cb.stats:
            #print('Now printing PDCP for RNTI', stat,'\n',monitor_handle.pdcp_cb.stats[str(stat)])
        time.sleep(update_interval)
        for rnti in MAC_cur_dl:
            #print(rnti)
            #print("MAC Received", MAC_cur_dl[str(rnti)]-MAC_old_dl[str(rnti)], "bytes in the last second - ", rnti)
            #print("MAC Transmitted", MAC_cur_ul[str(rnti)]-MAC_old_ul[str(rnti)], "bytes in the last second - ", rnti ,"\n")

            MAC_old_dl[str(rnti)] = MAC_cur_dl[str(rnti)]
            MAC_old_ul[str(rnti)] = MAC_cur_ul[str(rnti)]

        for rnti in PDCP_cur_tx:
            #print(rnti)
            #print("PDCP transmitted", PDCP_cur_tx[str(rnti)]-PDCP_old_tx[str(rnti)], "bytes in the last second - ", rnti)
            #print("PDCP Received", PDCP_cur_rx[str(rnti)]-PDCP_old_rx[str(rnti)], "bytes in the last second - ", rnti ,"\n")

            PDCP_old_tx[str(rnti)] = PDCP_cur_tx[str(rnti)]
            PDCP_old_rx[str(rnti)] = PDCP_cur_rx[str(rnti)]


        #print(monitor_handle.mac_cb.stats)
            #print("wat")
        
        # cur_cqi_mac = monitor_handle.mac_cb.stats[rnti]['wb_cqi']
        # cur_data_rlc_tx = monitor_handle.rlc_cb.stats[rnti]['txpdu_bytes']
        # cur_data_rlc_rx = monitor_handle.rlc_cb.stats[rnti]['rxpdu_bytes']
        # cur_data_pdcp_tx = monitor_handle.pdcp_cb.stats[rnti]['txpdu_bytes']
        # cur_data_pdcp_rx = monitor_handle.pdcp_cb.stats[rnti]['rxpdu_bytes']
        # print("MAC : ", cur_cqi_mac, "\nRLC : ", cur_data_rlc_tx, cur_data_rlc_rx, '\nPDCP : ',cur_data_pdcp_tx, cur_data_pdcp_rx)


        

    exit()


