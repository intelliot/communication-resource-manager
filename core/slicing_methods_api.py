CONTAINER=1
if CONTAINER == 1:
    from ..core import util
else:
    import util


class static_slicing(object):

    def __init__(self, monitor_handle, slice, slice_definitions, slicing_method_name, rbs, minimum_size):
        super().__init__()
        self.types = ['static_rbs', 'static'] # Defines if we should adapt the RB allocation
        #self.type = "static rbs"
        self.monitor_handle=monitor_handle
        self.max_rbs= rbs # Need to extract this somehow
        self.slice = slice
        self.slice_definitions = slice_definitions
        self.slicing_method_name = slicing_method_name
        self.minimum_size = minimum_size
        return

    def run(self):
        return

    def shutdown(self):
        return

    def generate_slice_def(self, min_throughput_mbps_downlink, min_throughput_mbps_uplink, rnti): # slice definitions + UE assoc json format
        #print(min_throughput_mbps_downlink)
        slice_id = util.get_next_id(self.slice) # TODO fix for smarter way #num_slice 
        print(slice_id)
        print(self.slice)
        self.slice["num_slices"]=int(len(self.slice["slices"]))+1
        if self.slicing_method_name == 'static':
            mcs = self.monitor_handle.mac_cb.stats[str(rnti)]['dl_mcs1']
            rbs = int(util.get_rbs_needed(mcs, min_throughput_mbps_downlink)) #ISSUE uplink yet... 
        if self.slicing_method_name == 'static_rbs':
            rbs = int(min_throughput_mbps_downlink) # Debugging
        if rbs == -1:
            "Could not assign"
            return -1, -1
        if rbs < self.max_rbs:
            new_high = int(self.slice["slices"][0]["slice_algo_params"]["pos_high"])
            new_low = int(new_high - rbs)
            if new_low > self.minimum_size: # We need at least 1 RB for the first slice TODO failure message if not possible
                old_high = new_low - 1
                self.slice["slices"][slice_id-2]["slice_algo_params"]["pos_high"]=old_high
            else:
                return -1, -1
                # TODO validate if valid
            new_slice = self.fill_slice(int(slice_id), "s"+str(slice_id+1), int(new_low), int(new_high))
            self.slice_definitions[str(slice_id)]=new_slice
        else:
            print("does not work - not enough RBs ")
            return -1, -1
        
        #self.reservations[int(slice_id)] = new_slice
        self.slice["slices"].append(new_slice)
        #print("Slices \n", self.slice["slices"])
        return self.slice, slice_id

    def order_reservations(self, slice_dict):
        # Find all allocated RB groupings
        # Push them towards the end, and allow the best effort to occupy the beginning
        RB_groups = []
        #print(" We have ",len(slice_dict['slices'])-1, " Slices")
        for i in range(len(slice_dict['slices'])-1): # We don't want to take the best effort slice
            RB_groups.append(slice_dict['slices'][i+1]["slice_algo_params"]["pos_high"] - slice_dict['slices'][i+1]["slice_algo_params"]["pos_low"])

        #print(RB_groups, sum(RB_groups)) # debugging + sanity check
        slice_dict['slices'][0]["slice_algo_params"]["pos_high"] =  self.max_rbs
        for i in range(len(slice_dict['slices'])-1):
            slice_dict['slices'][0]["slice_algo_params"]["pos_high"] =  self.max_rbs - RB_groups[i] - 1
            slice_dict['slices'][i+1]["slice_algo_params"]["pos_low"] =  slice_dict['slices'][0]["slice_algo_params"]["pos_high"] + 1
            slice_dict['slices'][i+1]["slice_algo_params"]["pos_high"] = slice_dict['slices'][i+1]["slice_algo_params"]["pos_low"] + RB_groups[i]
        return 

    def modify(self, slice_id, rbs, slice_dict): # nothing done on the reservation, we change the slice to better accomodate the latency - may be relevant for the VR traffic
        modfiable_high=slice_dict['slices'][slice_id]["slice_algo_params"]["pos_high"]
        modfiable_low=slice_dict['slices'][slice_id]["slice_algo_params"]["pos_low"]
        modfiable_low = modfiable_high - rbs
        slice_dict['slices'][slice_id]["slice_algo_params"]["pos_low"] = modfiable_low
        slice_dict['slices'][0]["slice_algo_params"]["pos_high"] = modfiable_low-1
        #print (self.slice['slices'][slice_id])
        return slice_dict

    def get_clean_slice(max_rbs):
        static_slice_def= {
            "num_slices" : 1,
            "slice_sched_algo" : "STATIC",
            "slices" : [
                {
                    "id" : 0,
                    "label" : "s1",
                    "ue_sched_algo" : "PF",
                    "slice_algo_params" : {"pos_low" : 0, "pos_high" : max_rbs},
                }
            ]
        }
        return static_slice_def

    def fill_slice(self, slice_id, label, low, high ):
        slice_def  = {
                        "id" : slice_id,
                        "label" : label, #TODO ?
                        "ue_sched_algo" : "PF",
                        "slice_algo_params" : {"pos_low" : low, "pos_high" : high},
            }

        return slice_def
    
    def reset_slice(self,slice):
        self.slice = slice
        self.slice_definitions = {}


class nvs_slicing(object):

    def __init__(self, reference_rate,monitor_handle, slice, slice_definitions, minimum_size):
        self.reference_rate = reference_rate # Might need to be initial input
        self.monitor_handle = monitor_handle
        self.slice=slice
        self.slice_definitions=slice_definitions
        self.minimum_size = int(minimum_size)
        return

    def run(self):
        return

    def shutdown(self):
        return


    def generate_slice_def(self, min_throughput_mbps_downlink, min_throughput_mbps_uplink, rnti): # slice definitions + UE assoc json format
        slice_id = util.get_next_id(self.slice) # TODO fix for smarter way #num_slice 
        self.slice["num_slices"]=int(len(self.slice["slices"]))+1

        dl_min_rate = min_throughput_mbps_downlink
        current_default_slice_rate = int(self.slice["slices"][0]["slice_algo_params"]["mbps_rsvd"])
        new_default_slice_rate = current_default_slice_rate - dl_min_rate
        if new_default_slice_rate < self.minimum_size:
            print("too much allocated, only ", self.reference_rate, " We need at least", self.minimum_size, 'on best effort, but we now have', new_default_slice_rate)
            return -1, -1
        
        # if dl_min_rate > self.reference_rate:
        #     print("Cannot assign more than reference rate")
        #     return -1
        # aggregate_rate = 0
        # for slice in self.slice_definitions:
        #     aggregate_rate = aggregate_rate + slice["slice_algo_params"]["mbps_rsvd"]
        # if aggregate_rate > self.reference_rate:
        #     print("we reserved too much across all slices compared to reference")
        #     return -1
        self.slice["slices"][0]["slice_algo_params"]["mbps_rsvd"]= new_default_slice_rate
        new_slice = self.fill_slice(int(slice_id), "s"+str(slice_id+1), int(dl_min_rate), int(self.reference_rate))
        self.slice_definitions[str(slice_id)]=new_slice
        self.slice["slices"].append(new_slice)
        return self.slice, slice_id


    def modify(self, slice_id, new_rate, slice_dict): # nothing done on the reservation, we change the slice to better accomodate the latency - may be relevant for the VR traffic
        old_rate=slice_dict['slices'][slice_id]["slice_algo_params"]["mbps_rsvd"]
        
        # if available rate : 
        slice_dict['slices'][slice_id]["slice_algo_params"]["mbps_rsvd"] = new_rate
        slice_dict['slices'][0]["slice_algo_params"]["mbps_rsvd"] = slice_dict['slices'][0]["slice_algo_params"]["mbps_rsvd"] + (old_rate - new_rate)  
        #print (self.slice['slices'][slice_id])
        return slice_dict


    def order_reservations(self, slice_dict):
        # Find all allocated RB groupings
        # Push them towards the end, and allow the best effort to occupy the beginning
        rate_groups = []
        for i in range(len(slice_dict['slices'])-1): # We don't want to take the best effort slice
            rate_groups.append(slice_dict['slices'][i+1]["slice_algo_params"]["mbps_rsvd"])
        #print(rate_groups, sum(rate_groups)) # debugging + sanity check
        slice_dict['slices'][0]["slice_algo_params"]["mbps_rsvd"] =  self.reference_rate - sum(rate_groups)


    def get_clean_slice(reference_rate):
        nvs_slice_def = {
            "num_slices" : 1,
            "slice_sched_algo" : "NVS",
            "slices" : [
                {
                    "id" : 0,
                    "label" : "s1",
                    "ue_sched_algo" : "PF",
                    "type" : "SLICE_SM_NVS_V0_RATE",
                    "slice_algo_params" : {"mbps_rsvd" : reference_rate, "mbps_ref" : reference_rate},
                }
            ]
        }
        return nvs_slice_def

    def fill_slice(self, slice_id, label, min_rate, ref ):
        slice_def  = {
                        "id" : slice_id,
                        "label" : label, #TODO ?
                        "ue_sched_algo" : "PF",
                        "type" : "SLICE_SM_NVS_V0_RATE",
                        "slice_algo_params" : {"mbps_rsvd" : min_rate, "mbps_ref" : ref},
            }
        return slice_def
    
    def reset_slice(self, slice):
        self.slice = slice
        self.slice_definitions = {}
