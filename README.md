## Building and running the docker container

To build the image run ```"./build.sh"``` this will create the image. This will require internet to install needed dependencies

After making the image, the container should be runnable with ```docker-compose up```.
This will start a python flask server in the container, which supports the API calls defined in commrm-api.yaml.
Relevant environmental variables are REFERENCE_RATE_VAL and MIN_BEST_EFFORT_RATE_VAL. The former must be set to whatever can be send in downlink to the UE at most. as slice requests uses a fraction of this amount. The latter is to maintain a certain bandwidth on the best effort slice, which cannot be allocated to others.
Make sure to also adapt the neccesary docker network, and the correct flexRIC IP.

API defintion can be found in commrm-api.yaml

## Functionalities

The slice mechanism works for both 4G and 5G utilizing the NVS slicing as developed for the flexRIC. Currently only downlink slices are supported, and the latency measures are intended for future work specifically towards guarantees of VR frames that are sent over the air.

Due to limitations with relating IP to unique identifier in the RAN (RNTI), currently only a single UE can have its own slice, and this is simplified to be the first UE joining the network.

## Simple API tests

API can be called through application like postman, otherwise example ones from the terminal can be seen below


```curl -X POST --user :  http://$IP:8081/reservation -H'Content-Type:application/json' --data-raw '{"maxLatencyMillisUplink": 0.80, "addressType": "ipAddress", "maxJitterMillisUplink": 6.02, "maxLatencyMillisDownlink": 5.96, "maxJitterMillisDownlink": 5.63, "minThroughputMbpsDownlink": 5, "clientAddress": "192.168.55.55", "minThroughputMbpsUplink": 1.46}'```



Get single reservation test
```curl -X GET --user : http://$IP:8081/reservation/1```


Get All reservations test
```curl -X GET --user : http://$IP:8081/reservation```

Delete reservation test
```curl -X DELETE --user : http://$IP:8081/reservation/1```


## Running

To run the application, make sure a flexRIC is running and connected to a eNB or gNB. Only OpenAirInterface implementaitons should be suppported for this, and these must also have the flexRIC patch applied.

With a UE connected, or multiple, a network slice can be created that allocates part of the bandwidth to the first UE that joined the network.
